package datadriven;

import org.testng.annotations.DataProvider;

public class CepDataProvider {
    @DataProvider
    public Object[][] novoCep(){
        return new Object[][]{
                {"91060080","Rua Itiberê da Cunha"},
                {"94415590","Rua Érico Veríssimo"},
                {"94910170","Rua Papa João XXIII"},
                {"90540130","Rua General Couto de Magalhães"}
        };
    }
}
